Write-Host "Packing..."
Remove-Item .\dist\*.nupkg   
dotnet pack -o .\dist
Write-Host "Packed"

Write-Host "Installing package..."
Get-Item .\dist\*.nupkg
dotnet new -i .\dist\*.nupkg
Write-Host "Package installed"