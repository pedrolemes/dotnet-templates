using FluentAssertions;
using GenFu;
using Moq;
using Project49aa6f99.Core.Infra.Repositories;
using Project49aa6f99.Core.Models;
using Project49aa6f99.Core.UseCases;
using Xunit;

namespace Project49aa6f99.UnitTests.Core.UseCases
{
    public class GetTodoItemsUseCaseTest
    {
        [Fact]
        public void GetAllAsync_Should_return_all_todo_items()
        {
            // Arrange
            var items = A.ListOf<TodoItem>(5);

            var todoRepositoryMock = new Mock<ITodoRepository>();
            todoRepositoryMock.Setup(x => x.GetAllAsync()).ReturnsAsync(items);

            var useCase = new GetTodoItemsUseCase(todoRepositoryMock.Object);

            // Act
            var result = useCase.GetAllAsync().Result;

            // Assert
            result.Should().BeEquivalentTo(items);
        }
    }
}