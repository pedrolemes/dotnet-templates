using FluentAssertions;
using Moq;
using Project49aa6f99.Core.Infra.Repositories;
using Project49aa6f99.Core.Models;
using Project49aa6f99.Core.UseCases;
using Xunit;

namespace Project49aa6f99.UnitTests.Core.UseCases
{
    public class AddTodoItemUseCaseTest
    {
        [Fact]
        public void AddAsync_Should_generate_new_id_and_insert_item()
        {
            // Arrange
            var item = new TodoItem
            {
                Completed = false,
                Description = "Test"
            };

            var todoRepositoryMock = new Mock<ITodoRepository>();

            var useCase = new AddTodoItemUseCase(todoRepositoryMock.Object);

            // Act
            useCase.AddAsync(item).Wait();

            // Assert
            todoRepositoryMock.Verify(x => x.AddAsync(item), Times.Once);
            item.Id.Should().NotBeEmpty();
        }
    }
}