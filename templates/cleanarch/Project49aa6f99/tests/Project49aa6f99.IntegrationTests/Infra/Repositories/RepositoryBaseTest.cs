using Project49aa6f99.Infra.Configurations;
using Project49aa6f99.Infra.Database;

namespace Project49aa6f99.IntegrationTests.Infra.Repositories
{
    public abstract class RepositoryBaseTest
    {
        public IDatabaseConnectionFactory ConnectionFactory => databaseConnectionFactory;

        IDatabaseConnectionFactory databaseConnectionFactory;

        public RepositoryBaseTest()
        {
            var inMemoryDatabase = new InMemoryDatabase(new DatabaseConfiguration());
            databaseConnectionFactory = new DatabaseConnectionFactory(inMemoryDatabase);
        }
    }
}