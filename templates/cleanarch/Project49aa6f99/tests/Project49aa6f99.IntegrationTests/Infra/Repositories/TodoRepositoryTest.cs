using System.Threading.Tasks;
using FluentAssertions;
using GenFu;
using Project49aa6f99.Core.Models;
using Project49aa6f99.Infra.Repositories;
using Xunit;

namespace Project49aa6f99.IntegrationTests.Infra.Repositories
{
    public class TodoRepositoryTest : RepositoryBaseTest
    {
        [Fact]
        public async Task AddAsync_Should_add_new_item_in_database()
        {
            // Arrange
            var items = A.ListOf<TodoItem>(5);

            var repository = new TodoRepository(base.ConnectionFactory);

            // Act
            foreach (var item in items)
            {
                await repository.AddAsync(item);
            }

            // Assert
            var itemsFromDatabase = await repository.GetAllAsync();

            itemsFromDatabase.Should().BeEquivalentTo(items);
        }

        [Fact]
        public async Task GetAllAsync_Should_return_all_items_from_database()
        {
            // Arrange
            var items = A.ListOf<TodoItem>(9);

            var repository = new TodoRepository(base.ConnectionFactory);

            foreach (var item in items)
            {
                await repository.AddAsync(item);
            }

            // Act
            var itemsFromDatabase = await repository.GetAllAsync();

            // Assert
            itemsFromDatabase.Should().BeEquivalentTo(items);
        }
    }
}