dotnet tool install dotnet-reportgenerator-globaltool --tool-path .tools   

Remove-Item -r .\.testResult\

dotnet test .\tests\Project49aa6f99.UnitTests\ --results-directory:".testResult\temp" --collect:"XPlat Code Coverage"
Move-Item .\.testResult\temp\*\coverage.cobertura.xml .\.testResult\unit.coverage.cobertura.xml

dotnet test .\tests\Project49aa6f99.IntegrationTests\ --results-directory:".testResult\temp" --collect:"XPlat Code Coverage"
Move-Item .\.testResult\temp\*\coverage.cobertura.xml .\.testResult\integration.coverage.cobertura.xml

Remove-Item -r .\.coverageReport\
.\.tools\reportgenerator "-reports:.\.testResult\*.cobertura.xml"  "-targetdir:.coverageReport" "-assemblyfilters:+Project49aa6f99.*" "-reporttypes:HtmlInline"

Remove-Item -r .\.testResult\
