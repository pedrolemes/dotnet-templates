﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Project49aa6f99.Core.Models;
using Project49aa6f99.Core.UseCases.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Project49aa6f99.WebApi.Models;
using AutoMapper;

namespace Project49aa6f99.WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TodoController : ControllerBase
    {
        private readonly IGetTodoItemsUseCase getTodoItemsUseCase;
        private readonly IAddTodoItemUseCase addTodoItemUseCase;
        private readonly IMapper mapper;
        private readonly ILogger<TodoController> _logger;

        public TodoController(
            IGetTodoItemsUseCase getTodoItemsUseCase,
            IAddTodoItemUseCase addTodoItemUseCase,
            IMapper mapper,
            ILogger<TodoController> logger)
        {
            this.getTodoItemsUseCase = getTodoItemsUseCase;
            this.addTodoItemUseCase = addTodoItemUseCase;
            this.mapper = mapper;
            _logger = logger;
        }

        [HttpGet]
        public async Task<IEnumerable<TodoItem>> GetAsync()
        {
            return await getTodoItemsUseCase.GetAllAsync();
        }

        [HttpPost]
        public async Task<IActionResult> PostAsync([FromBody] TodoItemRequestModel requestModel)
        {
            var item = mapper.Map<TodoItem>(requestModel); 
             await addTodoItemUseCase.AddAsync(item);
             return Ok(item);
        }
    }
}
