using AutoMapper;
using Project49aa6f99.Core.Models;
using Project49aa6f99.WebApi.Models;

namespace Project49aa6f99.WebApi.AutoMapper
{
    public class WebApiAutoMapperProfile : Profile
    {
        public WebApiAutoMapperProfile()
        {
            CreateMap<TodoItemRequestModel, TodoItem>();
        }
    }
}