namespace Project49aa6f99.WebApi.Models
{
    public class TodoItemRequestModel
    {
        public string Description { get; set; }
        public bool Completed { get; set; }
    }
}