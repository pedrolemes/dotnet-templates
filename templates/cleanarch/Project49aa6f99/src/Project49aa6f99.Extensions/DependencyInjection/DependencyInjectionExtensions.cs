using Project49aa6f99.Core.Infra.Repositories;
using Project49aa6f99.Core.UseCases;
using Project49aa6f99.Core.UseCases.Interfaces;
using Project49aa6f99.Infra.Repositories;
using Microsoft.Extensions.DependencyInjection;
using Project49aa6f99.Infra.Database;

namespace Project49aa6f99.Extensions.DependencyInjection
{
    public static class DependencyInjectionExtensions
    {
        public static IServiceCollection AddUseCases(this IServiceCollection services)
        {
            services.AddScoped<IGetTodoItemsUseCase, GetTodoItemsUseCase>();
            services.AddScoped<IAddTodoItemUseCase, AddTodoItemUseCase>();
            return services;
        }

        public static IServiceCollection AddRepositories(this IServiceCollection services)
        {
            services.AddDatabase();
            services.AddScoped<ITodoRepository, TodoRepository>();
            return services;
        }

        private static IServiceCollection AddDatabase(this IServiceCollection services)
        {
            services.AddSingleton<InMemoryDatabase>();
            services.AddSingleton<IDatabaseConnectionFactory, DatabaseConnectionFactory>();
            return services;
        }
    }
}