using Project49aa6f99.Infra.Configurations;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace Project49aa6f99.Extensions.Configuration
{
    public static class ConfigurationExtensions
    {
        public static IServiceCollection ConfigureDatabaseOptions(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<DatabaseConfiguration>(configuration.GetSection("Database"));
            services.AddSingleton<DatabaseConfiguration>(sp =>
               sp.GetRequiredService<IOptions<DatabaseConfiguration>>().Value);
            return services;
        }
    }
}