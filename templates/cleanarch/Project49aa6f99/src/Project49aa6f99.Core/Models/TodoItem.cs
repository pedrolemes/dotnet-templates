using System;

namespace Project49aa6f99.Core.Models
{
    public class TodoItem
    {
        public Guid Id { get; set; }
        public string Description { get; set; }
        public bool Completed { get; set; }
    }
}