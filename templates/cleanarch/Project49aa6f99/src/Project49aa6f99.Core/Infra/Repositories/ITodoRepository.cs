using System.Collections.Generic;
using System.Threading.Tasks;
using Project49aa6f99.Core.Models;

namespace Project49aa6f99.Core.Infra.Repositories
{
    public interface ITodoRepository
    {
         Task<IEnumerable<TodoItem>> GetAllAsync();
         Task AddAsync(TodoItem item);
    }
}