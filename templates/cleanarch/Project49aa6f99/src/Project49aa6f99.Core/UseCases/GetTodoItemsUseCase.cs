using System.Collections.Generic;
using System.Threading.Tasks;
using Project49aa6f99.Core.Infra.Repositories;
using Project49aa6f99.Core.Models;
using Project49aa6f99.Core.UseCases.Interfaces;

namespace Project49aa6f99.Core.UseCases
{
    public class GetTodoItemsUseCase : IGetTodoItemsUseCase
    {
        private readonly ITodoRepository todoRepository;

        public GetTodoItemsUseCase(ITodoRepository todoRepository)
        {
            this.todoRepository = todoRepository;
        }

        public async Task<IEnumerable<TodoItem>> GetAllAsync()
        {
            return await todoRepository.GetAllAsync();
        }
    }
}