using System;
using System.Threading.Tasks;
using Project49aa6f99.Core.Infra.Repositories;
using Project49aa6f99.Core.Models;
using Project49aa6f99.Core.UseCases.Interfaces;

namespace Project49aa6f99.Core.UseCases
{
    public class AddTodoItemUseCase : IAddTodoItemUseCase
    {
        private readonly ITodoRepository todoRepository;

        public AddTodoItemUseCase(ITodoRepository todoRepository)
        {
            this.todoRepository = todoRepository;
        }

        public async Task AddAsync(TodoItem item)
        {
            item.Id = Guid.NewGuid();
            await todoRepository.AddAsync(item);
        }
    }
}