using System.Threading.Tasks;
using Project49aa6f99.Core.Models;

namespace Project49aa6f99.Core.UseCases.Interfaces
{
    public interface IAddTodoItemUseCase
    {
         Task AddAsync(TodoItem item);
    }
}