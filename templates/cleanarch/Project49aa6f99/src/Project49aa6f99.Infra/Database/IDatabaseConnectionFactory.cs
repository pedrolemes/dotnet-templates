using System.Data;

namespace Project49aa6f99.Infra.Database
{
    public interface IDatabaseConnectionFactory
    {
         IDbConnection GetConnection();
    }
}