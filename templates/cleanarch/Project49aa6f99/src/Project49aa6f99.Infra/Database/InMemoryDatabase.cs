using System;
using System.Data;
using Project49aa6f99.Infra.Configurations;
using ServiceStack.OrmLite;
using ServiceStack.OrmLite.Sqlite;

namespace Project49aa6f99.Infra.Database
{
    public class InMemoryDatabase
    {
        private readonly OrmLiteConnectionFactory dbFactory;

        public InMemoryDatabase(DatabaseConfiguration databaseConfiguration)
        {
            var connectionString = databaseConfiguration?.ConnectionString ?? ":memory:";

            dbFactory = new OrmLiteConnectionFactory(connectionString, SqliteOrmLiteDialectProvider.Instance);
            CreateDatabase();
        }

        public IDbConnection GetConnection() => dbFactory.OpenDbConnection();

        private void CreateDatabase()
        {
            using (var connection = GetConnection())
            {
                var createDatabaseScript =
                    @"CREATE TABLE TodoItems (
                    Id UNIQUEIDENTIFIER NOT NULL, 
                    Description VARCHAR(50) NOT NULL, 
                    Completed BIT NOT NULL);";

                connection.ExecuteSql(createDatabaseScript);
            }
        }
    }
}