using System.Data;

namespace Project49aa6f99.Infra.Database
{
    public class DatabaseConnectionFactory : IDatabaseConnectionFactory
    {
        private readonly InMemoryDatabase inMemoryDatabase;

        public DatabaseConnectionFactory(InMemoryDatabase inMemoryDatabase)
        {
            this.inMemoryDatabase = inMemoryDatabase;
        }

        public IDbConnection GetConnection() => inMemoryDatabase.GetConnection();
    }
}