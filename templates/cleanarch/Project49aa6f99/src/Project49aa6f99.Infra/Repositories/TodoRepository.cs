using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Dapper;
using Project49aa6f99.Core.Infra.Repositories;
using Project49aa6f99.Core.Models;
using Project49aa6f99.Infra.Database;

namespace Project49aa6f99.Infra.Repositories
{
    public class TodoRepository : ITodoRepository
    {
        private readonly IDatabaseConnectionFactory databaseConnectionFactory;

        public TodoRepository(IDatabaseConnectionFactory databaseConnectionFactory)
        {
            this.databaseConnectionFactory = databaseConnectionFactory;
        }

        public async Task AddAsync(TodoItem item)
        {
            using (var dbConnection = databaseConnectionFactory.GetConnection())
            {
                await dbConnection.ExecuteAsync(
                    "Insert into TodoItems (Id, Description, Completed) VALUES (@Id, @Description, @Completed)",
                    new
                    {
                        item.Id,
                        item.Description,
                        item.Completed
                    });
            }
        }

        public async Task<IEnumerable<TodoItem>> GetAllAsync()
        {
            using (var dbConnection = databaseConnectionFactory.GetConnection())
            {
                return await dbConnection.QueryAsync<TodoItem>("select * from TodoItems");
            }
        }
    }
}